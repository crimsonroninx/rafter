Rafter TODO
=============
- [x] Monorepo to handle core and other dependencies
- [x] A babel preset package
- [ ] An eslint preset package
- [ ] "create react app" style boilerplate
- [ ] Example of middleware being autoloaded
- [ ] An example of a "modern" app eg using React, GraphQL Api