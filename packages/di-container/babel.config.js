module.exports = {
  presets: [['@rafter/babel-preset-rafter']],
  plugins: [['@babel/plugin-proposal-decorators', { legacy: true }]],
};
