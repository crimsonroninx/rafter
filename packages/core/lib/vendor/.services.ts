export default {
  express: {
    path: `${__dirname}/ExpressFactory`,
    dependencies: [],
  },
  diContainer: {
    path: `${__dirname}/BoxDiFactory`,
    dependencies: [],
  },
};
