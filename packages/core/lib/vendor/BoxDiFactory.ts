import { Box, IDiContainer } from '@rafter/di-container';

export default (): IDiContainer => {
  return Box;
};
