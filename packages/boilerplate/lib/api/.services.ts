export default {
  apiController: {
    path: `${__dirname}/ApiController`,
    dependencies: ['config.version'],
  },
};
