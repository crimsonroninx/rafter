export default {
  otherController: {
    path: `${__dirname}/OtherController`,
    dependencies: ['config.message'],
  },
};
